#!/usr/bin/python3 -u

import os
import sys
from argparse import ArgumentParser
from hashlib import sha1
from time import time
from struct import pack, unpack
import lmdb  # apt install python3-lmdb


def pipe_if_unseen(data, since, unique, no_update):
    s = sha1(data.encode())
    s.update(unique.encode())
    d = s.digest()

    env = lmdb.open(os.path.join(os.path.expanduser("~/.cache/pipe-if-unseen_lmdb/")))
    with env.begin(write=True) as txn:
        try:
            t = unpack(">i", txn.get(d))[0]  # unpack epoch bytes to int
        except:
            t = 0

        if t + since < time():
            sys.stdout.write(data)
            if not no_update:
                txn.put(d, pack(">i", int(time())))  # store epoch packed as bytes


def parse_args():
    parser = ArgumentParser(
        description="Pipe stdin to stdout if it hasn't already been seen."
    )
    parser.add_argument(
        "--since",
        "-s",
        type=int,
        help="Only pass output that hasn't been seen in X seconds.",
    )  # new version of old 1st arg
    parser.add_argument(
        "--unique",
        "-u",
        type=str,
        help="Specify unique string for running separately on the same input.",
    )  # new version of old optional 2nd arg
    parser.add_argument(
        "--per-line",
        "-l",
        action="store_true",
        help="Examine output on a per-line basis.",
    )
    parser.add_argument(
        "--no-update",
        "-n",
        action="store_true",
        help="Do not record any new output as seen.",
    )

    args = parser.parse_args()

    if not (args.since):
        parser.print_help()
        sys.exit()

    return args


def main():
    args = parse_args()

    data = sys.stdin.read()

    if args.per_line:
        for line in data.split("\n"):
            pipe_if_unseen(
                line + "\n",
                args.since,
                args.unique or "",
                args.no_update,
            )
    else:
        pipe_if_unseen(
            data,
            args.since,
            args.unique or "",
            args.no_update,
        )


if __name__ == "__main__":
    main()
