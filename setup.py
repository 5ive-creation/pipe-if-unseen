#!/usr/bin/env python
from setuptools import setup

setup(
    name="pipe_if_unseen",
    version="0.0.1",
    author="tkooda",
    description="Pipe stdin to stdout if it hasn't already been seen",
    packages=["pipe_if_unseen"],
    install_requires=["lmdb"],
    entry_points={
        "console_scripts": ["pipe-if-unseen=pipe_if_unseen.pipe_if_unseen:main"]
    },
)
